## automk: An Automatic Makefile.include Generator

automk's only external dependency is the docopt nim package, which can be
installed through nimble.

automk uses a configuration file in JSON format, an example is shown
below:

```
{
    "project": {
        "name": "your-project-name",
        "version": "1.2.3",
        "root": "..",
    },
    
    "sources": {
        "ignore": [
            "./.kdev4",
            "./.git",
            "./.idea"
        ]
    },
    
    "includes": [
        "./lib/",
        "./src/",
        "./other/relative/paths/for/your/headers"
    ],
    
    "output": {
        "destination": "./build_directory",
        "name": "Makefile.include"
    },
    
}

```

All paths are relative to the `sources.root` path. the `sources.root` path
is relative to wherever automk is being run from. In addition to the config
JSON file, automk can take the following command line arguments:

```
Usage:
    automk [--conf=<config_file>] [-d | --debug]
    automk (-h | --help)
    automk (-v | --version)

Options:
    --conf=<config_file>    Config file path [default: mkconf.json]
    -d --debug              Show debug messages.
    -h --help               Shows this screen.
    -v --version            Show version.
```

Here is a stripped-down version of automk's output. By default, its output is
written to a file named Makefile.include, but this can be changed in the 
config JSON. This output is based on the config file shown above:

```
PROJECT_ROOT=..

PROJECT_NAME=your-project-name

PROJECT_VERSION=1.2.3

vpath %.c $(PROJECT_ROOT)/lib/Newlib/
vpath %.c $(PROJECT_ROOT)/lib/CMSIS/
vpath %.cc $(PROJECT_ROOT)/lib/some_library/some_folder_with_cc_files/
vpath %.cc $(PROJECT_ROOT)/src/
vpath %.cpp $(PROJECT_ROOT)/src/Application/
vpath %.cc $(PROJECT_ROOT)/other/relative/paths/for/your/headers/


C_INCLUDES = \
-I$(PROJECT_ROOT)/lib/\
-I$(PROJECT_ROOT)/src/\
-I$(PROJECT_ROOT)/other/relative/paths/for/your/headers/


C_SOURCE_FILES = \
syscalls.c\
some_c.c\
source_files.c


CXX_SOURCE_FILES = \
various.cpp\
c_plus_plus.cc\
source.cxx\
files.cpp

```

automk recognizes .h, .hh, .hxx and .hpp as header file types. It recognizes .c
as a C source file, and it recognizes .cc, .cxx, and .cpp as C++ source files.
It recognizes .s and .S files as assembly. If your makefile does an object dump,
make sure the build folder is in automk's ignored directories. Otherwise, it will
try to list the objdump output (assuming it uses a .s or .S suffix) as an assembly
file to build.

An automk JSON file can be put anywhere in the filesystem, and it will be able to
work. Of course, it makes sense to put it in your project root or a build directory,
but the option is there. Paths in the output Makefile.include will always be
relative to the output directory it is put in.

