# automk: a makefile.include generator 
# version 0.1: Initial release                             9/25/2018                                                 
# version 0.3: Bugfixes, Windows Support, Versatility      9/26/2018                                                 
#       - Fixed bug where generated paths in Windows                                                                 
#         used backslashes instead of makefile-friendly                                                              
#         forward slashes                                                                                            
#       - Fixed crash caused by config files not located                                                             
#         in the call directory                                                                                      
#       - sources.root is now project.root in mkconf.json,                                                           
#         this helps indicate that it is special, since all                                                          
#         other paths in the mkconf file are relative to it                                                          
#       - added support for any and all hierarchies: mkconf                                                          
#         can be anywhere, output can be anywhere, and the                                                           
#         paths should still turn out correct.                                                                       

# TODO pre-version 1.0:                                                                                              
#       - thoroughly test ill-formatted mkconfs                                                                      
#       - add option to create a new template mkconf in                                                              
#         current directory                                                                                          
#       - make error messages print to stderr                                                                        

import os, re, json, strutils, sequtils, docopt                                                                      

# docopt automatically generates command line argument parser from                                                   
# this multiline string. Refer to docopt documentation on how it                                                     
# all works.                                                                                                         

let doc = """
automk: an automatic makefile.include generator

Usage:
    automk [--conf=<config_file>] [-d | --debug]
    automk (-h | --help)
    automk (-v | --version)

Options:
    --conf=<config_file>    Config file path [default: mkconf.json]
    -d --debug              Show debug messages.
    -h --help               Shows this screen.
    -v --version            Show version.
"""

import os, re, json, strutils, sequtils, docopt

let args = docopt(doc, version = "automk v0.3")
let config_file = expandFilename "$#".format(args["--conf"]) 
let show_debug = args["--debug"]

# read in the config file                                                                                            
if not existsFile config_file:                                                                                       
    echo "ERROR: " & config_file & " was not found."                                                                 
    echo "this might be helpful: automk --help"
    quit 1
elif show_debug:
    echo "NOTE: config file pathspec is: " & config_file  

var jsonNode: JsonNode
try:
    jsonNode = parseFile "mkconf.json"
except JsonParsingError:
    echo "ERROR: formatting error in config file (" & config_file & "):"
    echo getCurrentExceptionMsg()
    quit 1

type
    Project = object
        name: string
        version: string
        root: string
    Sources = object
        ignore: seq[string]
    Output = object
        destination: string
        name: string
    Config = object
        project: Project
        sources: Sources
        includes: seq[string]
        output: Output
var config: Config

config.project.name =    jsonNode["project"]["name"].getStr("application")
config.project.version = jsonNode["project"]["version"].getStr("")
config.project.root =    expandFilename jsonNode["project"]["root"].getStr(".")
try:                                                                                                                 
    if show_debug:                                                                                                   
        echo "NOTE: setting working directory to " & config.project.root                                             
        echo "    dirchange called for source/include file search"                                                   
    setCurrentDir config.project.root                                                                                
except OSError:                                                                                                      
    echo "ERROR: project.root: path not found (", config.project.root, ")"                                           
    echo "this might be helpful: automk --help"                                                                      
    quit 1

config.sources.ignore = foldl(jsonNode["sources"]["ignore"].getElems(), a & getStr b, config.sources.ignore)

config.output.destination = expandFilename(jsonNode["output"]["destination"].getStr("."))
config.output.name =        jsonNode["output"]["name"].getStr("Makefile.include")

var has_includes = false
try:
    config.includes = foldl(jsonNode["includes"].getElems(), a & getStr b, config.includes)
    has_includes = true
except KeyError:
    if show_debug:
        echo "NOTE: \"includes\" field was not found in mkconf. include pathspecs will be automatically generated."

proc tryExpand(toexpand: string): string =
    try:
        return expandFilename toexpand
    except OSError:
        if show_debug:
            echo "WARNING: failed to resolve sources.ignore pathspec (", toexpand, ")"
        return ""

config.sources.ignore = filter(map(config.sources.ignore, tryExpand), proc(x: string): bool = x.len > 0)

proc walkFilesFiltered(root: string, filter: seq[string]): seq[string] =
    var current: seq[string] = @[]
    for _, file in walkDir root:
        let file = expandFilename file
        var ok = true
        for ignore in config.sources.ignore:
            if contains(file, ignore):
                if show_debug:
                    echo "NOTE: filtered pathspec: ", file
                    echo "    from filter: ", ignore
                ok = false
        if ok:
            if existsDir file:
                current = concat(current, walkFilesFiltered(file, filter))
            else:
                current.add(file)
    return current

# sequence that will hold our vpath list
var vpaths: seq[string] = @[]

# c++ source file names
var cxx_sources: seq[string] = @[]

# c source file names
var c_sources: seq[string] = @[]

# asm source file names
var s_sources: seq[string] = @[]

# paths to header files
var includes: seq[string] = @[]

for file in walkFilesFiltered(".", config.sources.ignore):                                                           
    if file.match re".*\.(c)$":                                                                                      
        let (dir, name, ext) = splitFile file                                                                        
        if show_debug:                                                                                               
            echo "NOTE: found C source file " & dir & '/' & name & ext                                               
        vpaths = concat(vpaths, @["vpath %" & ext & " " & dir & '/'])
        c_sources = concat(c_sources, @[name & ext])
    elif file.match re".*\.(s|S)$":
        let (dir, name, ext) = splitFile file
        if show_debug:                                                                                               
            echo "NOTE: found ASM source file " & dir & '/' & name & ext                                             
        vpaths = concat(vpaths, @["vpath %" & ext & " " & dir & '/'])
        s_sources = concat(s_sources, @[name & ext])
    elif file.match re".*\.(cc|cpp|cxx)$":                                                                           
        let (dir, name, ext) = splitFile file                                                                        
        if show_debug:                                                                                               
            echo "NOTE: found C++ source file " & dir & '/' & name & ext                                             
        vpaths = concat(vpaths, @["vpath %" & ext & " " & dir & '/'])
        cxx_sources = concat(cxx_sources, @[name & ext])
    elif file.match re".*\.(h|hh|hpp|hxx)$":                                                                         
        let (dir, name, ext) = splitFile file                                                                        
        if show_debug:                                                                                               
            echo "NOTE: found header file " & dir & '/' & name & ext                                                 
        includes = concat(includes, @[dir & '/']) 

vpaths = deduplicate(vpaths)
c_sources = deduplicate(c_sources)
s_sources = deduplicate(s_sources)
cxx_sources = deduplicate(cxx_sources)

if has_includes:
    includes = deduplicate(map(config.includes, proc(x: string): string = (expandFilename x) & '/'))
else:
    includes = deduplicate(includes)

# write to output file
var outfile = open(config.output.destination & '/' & config.output.name, fmWrite)
var rootFull = expandFilename "."

# fix for windows- nim os module defaults to backslashes for Windows paths, replace them
# with makefile-friendly forward slashes
if DirSep != '/':
    rootFull = replace(rootFull, DirSep, '/')
    vpaths = map(vpaths, proc(x: string): string = replace x, DirSep, '/')
    c_sources = map(c_sources, proc(x: string): string = replace x, DirSep, '/')
    cxx_sources = map(cxx_sources, proc(x: string): string = replace x, DirSep, '/')
    includes = map(includes, proc(x: string): string = replace x, DirSep, '/')

# PROJECT_ROOT is printed as a path relative to the mkconf.json's project.root entry.
# however, config.project.root contains an absolute path. Compare the absolute paths
# of project.root and output.destination to figure out how many directories we need
# to step back through, relative to output.destination, in order to reach project.root
proc pathDepth(path: string): (int, string) =
    let pathspec = expandFilename path
    return (count(pathspec, DirSep), replace(pathspec, DirSep, '/'))

let (project_root_depth, project_root_fullpath) = pathDepth config.project.root
let (output_dest_depth, output_dest_fullpath) = pathDepth config.output.destination


var root_rel_to_output = "."
if output_dest_depth > project_root_depth:
    root_rel_to_output = repeat("../", output_dest_depth - project_root_depth)
    root_rel_to_output.removeSuffix('/')
elif output_dest_depth < project_root_depth:
    root_rel_to_output = root_rel_to_output & replace(project_root_fullpath, output_dest_fullpath, "")

outfile.writeLine("PROJECT_ROOT=" & root_rel_to_output & '\n')
outfile.writeLine("PROJECT_NAME=" & config.project.name & '\n')
if (len config.project.version) > 0:
    outfile.writeLine("PROJECT_VERSION=" & config.project.version & '\n')

for it in vpaths:
    if show_debug:
        echo "NOTE: creating vpath for " & it
    let it = it.replace(rootFull, "$(PROJECT_ROOT)")
    outfile.writeLine(it)

outfile.writeLine("\n\nC_INCLUDES = \\")
for index, it in includes:
    if show_debug:
        echo "NOTE: creating C_INCLUDES for " & it
    let it = it.replace(rootFull, "$(PROJECT_ROOT)")
    if index == (len includes) - 1:
        outfile.writeLine("-I" & it)
    else:
        outfile.writeLine("-I" & it & '\\')

outfile.writeLine("\n\nC_SOURCE_FILES = \\")
for index, it in c_sources:
    if show_debug:
        echo "NOTE: creating C_SOURCE_FILES for " & it
    if index == (len c_sources) - 1:
        outfile.writeLine(it)
    else:
        outfile.writeLine(it & '\\')

outfile.writeLine("\n\nCXX_SOURCE_FILES = \\")
for index, it in cxx_sources:
    if show_debug:
        echo "NOTE: creating CXX_SOURCE_FILES for " & it
    if index == (len cxx_sources) - 1:
        outfile.writeLine(it)
    else:
        outfile.writeLine(it & '\\')

outfile.writeLine("\n\nS_SOURCE_FILES = \\")
for index, it in s_sources:
    if show_debug:
        echo "NOTE: creating S_SOURCE_FILES for " & it
    if index == (len s_sources) - 1:
        outfile.writeLine(it)
    else:
        outfile.writeLine(it & '\\')
